# FinalProjectInvestree

Festick.com is a website that provides various kinds of concert tickets. Users can choose the ticket and the amount they want.

## Festick's features include:
### Users
- Landing page -> displays a carousel, several ticket list components, and an information component regarding festick.com facilities
- Register page -> displays the user registration form
- Login page -> displays the user login form
- Ticket List page -> displays the ticket list from festick.com
- Ticket Detail Page -> displays ticket details that have been selected by the user, here the user can enter the number of tickets desired and a total price calculation occurs according to the number of tickets entered
- Final Transaction Ticket page -> displays user order confirmation
- About Us page - displays the developer's profile and a glimpse of festick.com

### Admin
- Admin Dashboard page -> displays a summary of the amount of data in the festick.com database, starting from the list of users, tickets, catalogs, and transactions
- User List page -> displays the user list data, here the admin can read, update, and delete users
- Ticket List page -> displays ticket list data, here the admin can create, read, update, and delete tickets
- Catalog List page -> displays catalog list data, here the admin can create, read, update, and delete catalogs
- Transaction List page -> displays transaction list data, here the admin can create, read, update, and delete transactions
- Transaction Success List page -> displays data list transaction with success status
- Transaction Pending List page -> displays data list transactions with pending status
- Transaction Refund List page -> displays transaction list data with refund status